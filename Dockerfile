FROM ruby:2.6.5

RUN apt-get update -qq && apt-get install -y nodejs yarn

RUN mkdir /alitwitter
WORKDIR /alitwitter

COPY Gemfile /alitwitter/Gemfile
COPY Gemfile.lock /alitwitter/Gemfile.lock

RUN gem install bundler -v '2.1.4'
RUN bundle install

COPY . /alitwitter

EXPOSE 3000
CMD ["bundle", "exec", "rails", "server", "-b", "0.0.0.0"]
