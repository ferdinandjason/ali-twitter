## Description
As a user,
- I want to be able to create a tweet, so that I can express what is my mind.
- I want to be able delete a tweet, so that I can remove a tweet that I don't want to.
- I want to be able to see my tweets, so that I can read tweet I have created before.

## Environment Setup
- Ruby 2.6.5
- Bundler 2.1.4
- Rails 6.0.2.1

You can install rails using this command below:
```bash
gem install rails --version=6.0.2.1
```

Install all dependency using bundle in root of directory project.
```bash
bundle install
```

## Run Test
using rspec & rubocop
```bash
bundle exec rake
```

## Run Instruction
After clone this project, run :
```bash
yarn
```

Then migrate the database using this command :
```bash
rails db:migrate
```

Execute this command to run the server :
```bash
rails server
```

Open `http://localhost:3000/tweets` in browser

## Artifacts
### How to make an artifact
### Zip
1. Just zip this application/project

### Docker
1. I have prepared the Dockerfile
2. Build docker with
```
docker build -t alitwitter:lastest .
```
3. Run by
```
docker container run --publish 3000:3000 --detach --name twitter alitwitter:lastest
```
