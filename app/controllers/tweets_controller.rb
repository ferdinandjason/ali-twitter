class TweetsController < ApplicationController
  def index
    @tweets = Tweet.all
  end

  def new
    @tweet = Tweet.new
  end

  def create
    @tweet = Tweet.new(params.require(:tweet).permit(:body))
    if @tweet.valid?
      @tweet.save
      redirect_to tweets_path
    else
      flash[:errors] = @tweet.errors.full_messages
      redirect_to new_tweet_path
    end
  end

  def destroy
    @tweet = Tweet.find(params[:id])
    @tweet.destroy
    flash[:status] = 'Tweet was successfully deleted'
    redirect_to tweets_path
  end
end
